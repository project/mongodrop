<?php

function mongodrop_configure_fields() {
  module_enable(array('mongodb_field_storage'));
  drupal_flush_all_caches();
}

function mongodrop_install_tasks() {
  $task['field_storage_type'] = array(
    'display_name' => st('Enable MongoDB Field Storage'),
    'display' => FALSE,
    'type' => 'normal',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    'function' => 'mongodrop_configure_fields',
  );
  return $task;
}
