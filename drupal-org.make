api = 2
core = 7.14

; Build Kit drupal-org.make (Jan 17, 2013)

includes[] = http://drupalcode.org/project/buildkit.git/blob_plain/a9bd832efa4f217056176679a106d5b9d1fa7511:/drupal-org.make

; Build Kit Overrides

projects[tao][subdir] = contrib
projects[rubik][subdir] = contrib

; Contrib

projects[entity][subdir] = contrib
projects[entity][version] = 1.0

projects[mongodb][subdir] = contrib
projects[mongodb][type] = module
projects[mongodb][download][type] = git
projects[mongodb][download][url] = http://git.drupal.org/project/mongodb.git
projects[mongodb][download][branch] = 7.x-1.x
; Dec 13, 2012
projects[mongodb][download][revision] = f82618fc

; module_exists() not available in session.inc
; http://drupal.org/node/1412190#comment-5498556
; projects[mongodb][patch][] = http://drupal.org/files/module_exists_not_available-1412190-3.patch

; mongodb_block.module conflicts with block.module
; http://drupal.org/node/1163584#comment-5496150
; projects[mongodb][patch][] = http://drupal.org/files/block_conflict-1163584-11.patch

projects[search_api][subdir] = contrib
projects[search_api][version] = 1.4

projects[search_api_mongodb][subdir] = contrib
projects[search_api_mongodb][type] = module
projects[search_api_mongodb][download][type] = git
projects[search_api_mongodb][download][url] = http://git.drupal.org/project/search_api_mongodb.git
projects[search_api_mongodb][download][revision] = b736ccef

projects[search_api_page][subdir] = contrib
projects[search_api_page][version] = 1.0-beta2
