api = 2
core = 7.x

; Include Drupal core and any core patches from Build Kit
includes[] = http://drupalcode.org/project/buildkit.git/blob_plain/refs/heads/7.x-2.x:/drupal-org-core.make

projects[mongodrop][type] = profile
projects[mongodrop][download][type] = git
projects[mongodrop][download][url] = http://git.drupal.org/project/mongodrop.git
projects[mongodrop][download][branch] = master
